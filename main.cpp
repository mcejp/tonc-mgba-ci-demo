#include <tonc.h>

#include "mgba/mgba.h"

int main() {
#if BENCHMARK
    mgba_open();

    // pass 1 frame for patch to take place
    irq_init(nullptr);
    irq_enable(II_VBLANK);
    VBlankIntrWait();

    auto testcase_id = *(u16*)0x0203FFFE;
    mgba_printf(MGBA_LOG_INFO, "Requested test case: %04Xh", testcase_id);

    profile_start();

    // waste some time
    for (int i = 1; i <= 10; i++) {
        Div(i, i);
    }

    auto duration = profile_stop();

    mgba_printf(MGBA_LOG_INFO, "BENCHMARK: %d cycles", duration);

    Stop();
#else
    // Normal gameplay here...
#endif
}
